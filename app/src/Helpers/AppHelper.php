<?php

namespace App\src\Helpers;

class AppHelper
{

    public static function filterArrayByExistKey(array $array, array $existKeys): array
    {
        return array_filter($array, function ($key) use ($existKeys) {
            return in_array($key, $existKeys);
        }, ARRAY_FILTER_USE_KEY);
    }

}
