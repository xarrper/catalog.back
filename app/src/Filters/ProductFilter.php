<?php

namespace App\src\Filters;

class ProductFilter extends QueryFilter
{

    protected function search(string $value)
    {
        $this->builder->where('title', 'like', "%$value%");
    }

    protected function category(int $value)
    {
        $this->builder->where('category_id', $value);
    }

    protected function costFrom($value)
    {
        $this->builder->where('cost', '>=', (float)$value);
    }

    protected function costTo($value)
    {
        $this->builder->where('cost', '<=', (float)$value);
    }

}