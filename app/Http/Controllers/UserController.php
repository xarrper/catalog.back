<?php

namespace App\Http\Controllers;

use App\src\Models\User;
use App\src\Repositories\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $userRepository;

    function __construct(User $user)
    {
        $this->userRepository = new Repository($user);
    }

    public function find()
    {
        $user = $this->userRepository
            ->with('role')
            ->find(Auth::user()->id);

        return response()->json($user);
    }
}
