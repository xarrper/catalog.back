<?php

namespace App\Http\Controllers;

use App\src\Models\Category;
use App\src\Repositories\Repository;

class CategoryController extends Controller
{
    private $categoryRepository;

    function __construct(Category $categories)
    {
        $this->categoryRepository = new Repository($categories);
    }

    public function index()
    {
        $categories = $this->categoryRepository->all();
        return response()->json($categories);
    }

}
