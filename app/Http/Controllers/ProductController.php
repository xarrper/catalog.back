<?php

namespace App\Http\Controllers;

use App\Events\PurchaseEvent;
use App\src\Models\Category;
use App\Http\Requests\ProductRequest;
use App\src\Filters\ProductFilter;
use App\src\Models\Product;
use App\src\Models\User;
use App\src\Repositories\ProductRepository;
use App\src\Repositories\Repository;

class ProductController extends Controller
{

    private $categoryRepository;
    private $productRepository;

    function __construct(Category $categories, ProductRepository $productRepository)
    {
        $this->categoryRepository = new Repository($categories);
        $this->productRepository = $productRepository;
    }

    public function index(ProductFilter $filters)
    {
        $products = $this->productRepository->filter($filters);
        return response()->json($products);
    }

    public function show(Product $product)
    {
        $product = $this->productRepository
            ->with('category')
            ->find($product->id);

        return response()->json($product);
    }

    public function store(ProductRequest $request)
    {
        $product = $this->productRepository->create($request->all());
        return response()->json(['data' => $product, 'message' => 'Данные успешно добавлены'], 200);
    }

    public function update(ProductRequest $request, Product $product)
    {
        $product = $this->productRepository->update($request->all(), $product->id);
        return response()->json(['data' => $product, 'message' => 'Данные успешно изменены'], 200);
    }

    public function destroy(Product $product)
    {
        $result = $this->productRepository->delete($product->id);
        return response()->json(['data' => $result, 'message' => 'Данные успешно удалены'], 200);
    }

    public function buy(User $user)
    {
//        PurchaseEvent::dispatch($user->username);
//        return back();
    }

}
