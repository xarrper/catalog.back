<?php

Route::get('/categories', 'CategoryController@index');
Route::get('/products', 'ProductController@index');
Route::get('/products/{product}', 'ProductController@show');
Route::post('/login', 'LoginController@login');
Route::get('/getUser', 'UserController@find');
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::post('/logout', 'LoginController@logout');
    Route::get('/getUser', 'UserController@find');
    Route::post('/products', 'ProductController@store')->name('products.store');//->middleware('role:admin');
//    Route::post('/products/{user}/buy', 'ProductController@buy')->name('products.buy')->middleware('role:user');
    Route::put('/products/{product}', 'ProductController@update')->name('products.update');//->middleware('can:checkOwn,product');
    Route::delete('/products/{product}', 'ProductController@destroy')->name('products.destroy');//->middleware('can:checkOwn,product');
});
